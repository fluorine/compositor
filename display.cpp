#include "display.h"
#include "output.h"
#include <glbinding/gl/gl.h>
using namespace phasor;

Display::Display(std::shared_ptr<Output> output) :
    m_output(output)
{

}

void Display::render()
{
	m_output->start_rendering();
	gl::glClearColor(1.0f, 1.0f, 0.0f, 1.0f);
	gl::glClear(gl::GL_COLOR_BUFFER_BIT);
	m_output->finish_rendering();
}
