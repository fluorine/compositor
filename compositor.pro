TEMPLATE = app
CONFIG += console c++14 object_parallel_to_source
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lwayland-server -lSDL2 -lGL -lglbinding

protoXdgTargetCode.target = ../compositor/protocols/xdg_shell_v6.c
protoXdgTargetCode.commands = wayland-scanner code ../compositor/protocols/xdg-shell-unstable-v5.xml ../compositor/protocols/xdg_shell_v6.c
PRE_TARGETDEPS += ../compositor/protocols/xdg_shell_v6.c
QMAKE_EXTRA_TARGETS += protoXdgTargetCode

protoXdgTarget.target = ../compositor/protocols/xdg_shell_v6.h
protoXdgTarget.depends = FORCE
protoXdgTarget.commands = wayland-scanner server-header ../compositor/protocols/xdg-shell-unstable-v6.xml ../compositor/protocols/xdg_shell_v6.h
PRE_TARGETDEPS += ../compositor/protocols/xdg_shell_v6.h
QMAKE_EXTRA_TARGETS += protoXdgTarget

SOURCES += \
    main.cpp \
    geometry/ostream.cpp \
    geometry/rectangle.cpp \
    geometry/region.cpp \
    protocols/xdg_shell_v6.c \
    wayland/surface.cpp \
    wayland/compositor.cpp \
    wayland/client.cpp \
    compositor.cpp \
    platform.cpp \
    nestedplatform.cpp \
    display.cpp

HEADERS += \
    geometry/dimensions.h \
    geometry/displacement.h \
    geometry/length.h \
    geometry/point.h \
    geometry/rectangle.h \
    geometry/region.h \
    geometry/size.h \
    geometry/geometry.h \
    protocols/xdg_shell_v6.h \
    wayland/surface.h \
    wayland/compositor.h \
    wayland/client.h \
    compositor.h \
    platform.h \
    nestedplatform.h \
    display.h \
    output.h
