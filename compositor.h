#ifndef PHASOR_COMPOSITOR_H
#define PHASOR_COMPOSITOR_H

#include "wayland/compositor.h"
#include <memory>
#include <vector>

namespace phasor {

class NestedPlatform;
class Display;

class Compositor : public wayland::Compositor
{
public:
	Compositor();

	/**
	 * @brief Starts the compositor.
	 * @return The exit status.
	 */
	int run();

protected:
	/**
	 * @brief The main loop for the compositor.
	 * @return Returns false if the compositor is to terminate.
	 */
	bool main_loop();

	std::shared_ptr<NestedPlatform> m_platform;
	std::vector<std::shared_ptr<Display>> m_displays;
};

}

#endif // PHASOR_COMPOSITOR_H
