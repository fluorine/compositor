#include "region.h"

namespace pg = phasor::geometry;

pg::Region::Region()
{
	m_positives.reserve(2);
	m_negatives.reserve(2);
}

void pg::Region::operator +=(const pg::Rectangle &rhs)
{
	m_positives.push_back(rhs);
}

void pg::Region::operator -=(const pg::Rectangle &rhs)
{
	m_negatives.push_back(rhs);
}

auto pg::Region::contains(const Point &p) const -> bool
{
	bool found_positive = false;
	for (const pg::Rectangle &r : m_positives) {
		if (r.contains(p)) {
			found_positive = true;
			break;
		}
	}
	if (!found_positive)
		return false;

	for (const pg::Rectangle &r : m_negatives) {
		if (r.contains(p)) {
			return false;
		}
	}

	return true;
}
