#ifndef PHASOR_GEOMETRY_H
#define PHASOR_GEOMETRY_H

#include "dimensions.h"
#include "displacement.h"
#include "length.h"
#include "point.h"
#include "rectangle.h"
#include "region.h"
#include "size.h"

namespace pg = phasor::geometry;

#endif // PHASOR_GEOMETRY_H
