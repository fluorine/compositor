#ifndef PHASOR_GEOMETRY_DIMENSIONS_H_
#define PHASOR_GEOMETRY_DIMENSIONS_H_

// Heavily inspired by Mir's IntWrapper<>
// at "mir/src/include/common/mir/geometry/dimensions.h"

#include <iosfwd>

namespace phasor {
namespace geometry {

enum DimensionTag { width, height, x, y, dx, dy, stride };

template<DimensionTag tag>
class IntWrapper
{
public:
	typedef int ValueType;
	IntWrapper() : _value(0) {}

	template<typename AnyInteger>
	IntWrapper(AnyInteger value) : _value(static_cast<ValueType>(value)) { }

	int as_int() const { return _value; }
	float as_float() const { return _value; }

protected:
	ValueType _value;
};

typedef IntWrapper<width> Width;
typedef IntWrapper<height> Height;
typedef IntWrapper<x> X;
typedef IntWrapper<y> Y;
typedef IntWrapper<dx> DeltaX;
typedef IntWrapper<dy> DeltaY;
typedef IntWrapper<stride> Stride;

template<DimensionTag Tag>
std::ostream& operator<<(std::ostream& out, IntWrapper<Tag> const &value)
{
	out << value.as_int();
	return out;
}

template<DimensionTag Tag>
bool operator == (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() == rhs.as_int();
}

template<DimensionTag Tag>
bool operator != (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() != rhs.as_int();
}

template<DimensionTag Tag>
bool operator <= (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() <= rhs.as_int();
}

template<DimensionTag Tag>
bool operator >= (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() >= rhs.as_int();
}

template<DimensionTag Tag>
bool operator < (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() < rhs.as_int();
}

template<DimensionTag Tag>
bool operator > (IntWrapper<Tag> const &lhs, IntWrapper<Tag> const &rhs)
{
	return lhs.as_int() > rhs.as_int();
}

// Adding delta's of the same direction is allowed.
inline DeltaX operator+(DeltaX lhs, DeltaX rhs) { return DeltaX(lhs.as_int() + rhs.as_int()); }
inline DeltaX operator-(DeltaX lhs, DeltaX rhs) { return DeltaX(lhs.as_int() - rhs.as_int()); }
inline DeltaY operator+(DeltaY lhs, DeltaY rhs) { return DeltaY(lhs.as_int() + rhs.as_int()); }
inline DeltaY operator-(DeltaY lhs, DeltaY rhs) { return DeltaY(lhs.as_int() - rhs.as_int()); }

// Adding delta's to coordinates is allowed.
inline X operator+(X lhs, DeltaX rhs) { return X(lhs.as_int() + rhs.as_int()); }
inline X operator-(X lhs, DeltaX rhs) { return X(lhs.as_int() - rhs.as_int()); }
inline Y operator+(Y lhs, DeltaY rhs) { return Y(lhs.as_int() + rhs.as_int()); }
inline Y operator-(Y lhs, DeltaY rhs) { return Y(lhs.as_int() - rhs.as_int()); }

// Subtracting coordinates is allowed.
inline DeltaX operator-(X lhs, X rhs) { return DeltaX(lhs.as_int() - rhs.as_int()); }
inline DeltaY operator-(Y lhs, Y rhs) { return DeltaY(lhs.as_int() - rhs.as_int()); }

// Multiplying by scalars is allowed.
template<typename Scalar>
Width operator*(Scalar scale, Width const &w) { return Width(scale * w.as_int()); }
template<typename Scalar>
Height operator*(Scalar scale, Height const &h) { return Height(scale * h.as_int()); }
template<typename Scalar>
DeltaX operator*(Scalar scale, DeltaX const &dx) { return DeltaX(scale * dx.as_int()); }
template<typename Scalar>
DeltaY operator*(Scalar scale, DeltaY const &dy) { return DeltaY(scale * dy.as_int()); }

template<typename Scalar>
Width operator*(Width const &w, Scalar scale) { return scale * w; }
template<typename Scalar>
Height operator*(Height const &h, Scalar scale) { return scale * h; }
template<typename Scalar>
DeltaX operator*(DeltaX const &dx, Scalar scale) { return scale * dx; }
template<typename Scalar>
DeltaY operator*(DeltaY const &dy, Scalar scale) { return scale * dy; }

template<typename Target, typename Source>
inline Target dim_cast(Source s) { return Target(s.as_int()); }

}
}

#endif // PHASOR_GEOMETRY_DIMENSIONS_H_
