#ifndef PHASOR_GEOMETRY_DISPLACEMENT_H_
#define PHASOR_GEOMETRY_DISPLACEMENT_H_

#include "dimensions.h"
#include "point.h"
#include <iosfwd>

namespace phasor {
namespace geometry {

struct Displacement
{
    Displacement() { }
    Displacement(Displacement const&) = default;
    Displacement& operator=(Displacement const&) = default;

    template<typename DXType, typename DYType>
    Displacement(DXType&& dx, DYType&& dy) : dx(dx), dy(dy) { }

    long long length_squared() const
    {
        long long x = dx.as_int(), y = dy.as_int();
        return x * x + y * y;
    }

    DeltaX dx;
    DeltaY dy;
};

inline bool operator == (Displacement const& lhs, Displacement const& rhs)
{
    return lhs.dx == rhs.dx && lhs.dy == rhs.dy;
}

inline bool operator != (Displacement const& lhs, Displacement const& rhs)
{
    return lhs.dx != rhs.dx || lhs.dy != rhs.dy;
}

inline Displacement operator+(Displacement const& lhs, Displacement const& rhs)
{
    return Displacement(lhs.dx + rhs.dx, lhs.dy + rhs.dy);
}

inline Displacement operator-(Displacement const& lhs, Displacement const& rhs)
{
    return Displacement(lhs.dx - rhs.dx, lhs.dy - rhs.dy);
}

inline Point operator+(Point const& lhs, Displacement const& rhs)
{
    return Point(lhs.x + rhs.dx, lhs.y + rhs.dy);
}

inline Point operator-(Point const& lhs, Displacement const& rhs)
{
    return Point(lhs.x - rhs.dx, lhs.y - rhs.dy);
}

inline Displacement operator-(Point const& lhs, Point const& rhs)
{
    return Displacement(lhs.x - rhs.x, lhs.y - rhs.y);
}

inline bool operator < (Displacement const& lhs, Displacement const& rhs)
{
    return lhs.length_squared() < rhs.length_squared();
}

template<typename Scalar>
inline Displacement operator * (Scalar scale, Displacement const& d)
{
    return Displacement(scale * d.dx, scale * d.dy);
}

template<typename Scalar>
inline Displacement operator * (Displacement const& d, Scalar scale)
{
    return scale * d;
}

std::ostream& operator << (std::ostream& out, Displacement const& d);

#ifdef PHASOR_GEOMETRY_SIZE_H_
inline Displacement as_displacement(Size const& size)
{
    return Displacement(size.width.as_int(), size.height.as_int());
}

inline Size as_size(Displacement const& disp)
{
    return Size(disp.dx.as_int(), disp.dy.as_int());
}
#endif

}
}

#endif // PHASOR_GEOMETRY_DISPLACEMENT_H_
