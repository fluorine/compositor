#ifndef PHASOR_GEOMETRY_LENGTH_H_
#define PHASOR_GEOMETRY_LENGTH_H_

namespace phasor {
namespace geometry {

class Length
{
public:
    enum Units
    {
        micrometers = 1,
        millimeters = 1000,
        centimeters = 10000,
        inches = 25400
    };

    Length() : magnitude(0.0) { }
    Length(Length const&) = default;
    Length& operator=(Length const&) = default;
    Length(float mag, Units units) : magnitude(mag * (float)units) { }

    float as(Units units) const
    {
        return magnitude / static_cast<float>(units);
    }

    float as_pixels(float dpi) const
    {
        return as(inches) * dpi;
    }

    bool operator == (Length const& rhs)
    {
        return magnitude == rhs.magnitude;
    }

    bool operator != (Length const& rhs)
    {
        return magnitude != rhs.magnitude;
    }

    inline Length operator+(Length const& rhs)
    {
        return Length(magnitude + rhs.magnitude, Length::micrometers);
    }

    inline Length operator-(Length const& rhs)
    {
        return Length(magnitude - rhs.magnitude, Length::micrometers);
    }

private:
    float magnitude;
};


inline Length operator""_mm(long double mag)
{
    return Length(mag, Length::millimeters);
}

inline Length operator""_mm(unsigned long long mag)
{
    return Length(mag, Length::millimeters);
}

inline Length operator""_cm(long double mag)
{
    return Length(mag, Length::centimeters);
}

inline Length operator""_cm(unsigned long long mag)
{
    return Length(mag, Length::centimeters);
}

inline Length operator""_inches(long double mag)
{
    return Length(mag, Length::inches);
}

inline Length operator""_inches(unsigned long long mag)
{
    return Length(mag, Length::inches);
}

}
}

#endif // PHASOR_GEOMETRY_LENGTH_H_
