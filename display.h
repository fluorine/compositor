#ifndef DISPLAY_H
#define DISPLAY_H

#include "geometry/size.h"
#include <memory>

namespace phasor {

class Output;
class Display
{
public:
	Display(std::shared_ptr<Output> output);

	geometry::Size size() const { return m_size; }
	float dpi() const { return m_dpi; }

	void render();

protected:
	geometry::Size m_size;
	float m_dpi;

	std::shared_ptr<Output> m_output;
};

}

#endif // DISPLAY_H
