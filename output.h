#ifndef OUTPUT_H
#define OUTPUT_H

#include "geometry/size.h"

namespace phasor {

class Output
{
public:
	virtual geometry::Size size() const = 0;
	virtual float dpi() const = 0;

	virtual void start_rendering() = 0;
	virtual void finish_rendering() = 0;
};

}

#endif // OUTPUT_H
