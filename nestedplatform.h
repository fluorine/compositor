#ifndef NESTEDPLATFORM_H
#define NESTEDPLATFORM_H

#include <SDL2/SDL.h>

#include "platform.h"
#include "output.h"
#include "geometry/size.h"

namespace phasor {

class NestedOutput : public Output
{
public:
	NestedOutput(geometry::Size size, float dpi);

	geometry::Size size() const override { return m_size; }
	float dpi() const override { return m_dpi; }

	void initialize();

	virtual void start_rendering() override;
	virtual void finish_rendering() override;

protected:
	geometry::Size m_size;
	float m_dpi;

	SDL_Window *m_window;
	SDL_GLContext m_gl_context;
};

class NestedPlatform : public Platform
{
public:
	NestedPlatform(geometry::Size display_size);
	~NestedPlatform();

	void initialize() override;
	void finish_rendering() override;
	std::vector<std::shared_ptr<Output>>& outputs() override;

	bool should_quit() const { return m_should_quit; }

protected:
	geometry::Size m_size;

	bool m_should_quit;

	std::vector<std::shared_ptr<Output>> m_outputs;
};

}

#endif // NESTEDPLATFORM_H
