#include "nestedplatform.h"
#include <stdexcept>
using namespace phasor;
using namespace std;

NestedOutput::NestedOutput(geometry::Size size, float dpi) :
    m_size(size),
    m_dpi(dpi)
{
}

void NestedOutput::initialize()
{
	m_window = SDL_CreateWindow(
	            "Fluorine Compositor",
	            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	            m_size.width.as_int(),
	            m_size.height.as_int(),
	            SDL_WINDOW_OPENGL);

	if (!m_window) {
		throw runtime_error("Failed to create an SDL window.");
	}

	m_gl_context = SDL_GL_CreateContext(m_window);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
	                    SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_SetSwapInterval(1);

	// Get the right DPI
	int display_index = SDL_GetWindowDisplayIndex(m_window);
	SDL_GetDisplayDPI(display_index, nullptr, &m_dpi, nullptr);
}

void NestedOutput::start_rendering()
{
	SDL_GL_MakeCurrent(m_window, m_gl_context);
}

void NestedOutput::finish_rendering()
{
	SDL_GL_SwapWindow(m_window);
}

NestedPlatform::NestedPlatform(geometry::Size display_size) :
    m_size(display_size)
{

}

NestedPlatform::~NestedPlatform()
{

}

void NestedPlatform::initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
		throw runtime_error("Failed to initialize SDL.");
	}

	std::shared_ptr<NestedOutput> output = std::make_shared<NestedOutput>(m_size, 1.0f);
	output->initialize();
	m_outputs.push_back(output);
}

void NestedPlatform::finish_rendering()
{
}

std::vector<std::shared_ptr<Output>> &NestedPlatform::outputs()
{
	return m_outputs;
}

