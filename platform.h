#ifndef PLATFORM_H
#define PLATFORM_H

#include "geometry/size.h"
#include <vector>
#include <memory>

namespace phasor {

class Output;

class Platform
{
public:
	virtual void initialize() = 0;
	virtual void start_rendering() {}
	virtual void finish_rendering() = 0;

	virtual std::vector<std::shared_ptr<Output>>& outputs() = 0;
};

}


#endif // PLATFORM_H
