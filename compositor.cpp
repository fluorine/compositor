#include "compositor.h"
#include "display.h"
#include "nestedplatform.h"
using namespace phasor;

Compositor::Compositor()
{
	create_display();
}

int Compositor::run()
{
	m_platform = std::make_shared<NestedPlatform>(geometry::Size { 640, 480 });
	m_platform->initialize();

	// Create the displays
	for (std::shared_ptr<Output> output : m_platform->outputs()) {
		std::shared_ptr<Display> display = std::make_shared<Display>(output);
		m_displays.push_back(display);
	}

	while(main_loop()) ;
	return 0;
}

bool Compositor::main_loop()
{
	dispatch_events();

	for (std::shared_ptr<Display> display : m_displays) {
		display->render();
	}

	// Get input events
	return true;
}
