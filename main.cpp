#include <iostream>
#include <SDL2/SDL.h>
#include <glbinding/Binding.h>
#include <glbinding/gl/gl.h>

#include "geometry/geometry.h"
#include "compositor.h"

using namespace std;
using namespace phasor;

int main(int argc, char *argv[])
{
	glbinding::Binding::initialize();

	Compositor c;
	return c.run();
}
