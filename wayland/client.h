#ifndef PHASOR_WAYLAND_CLIENT_H
#define PHASOR_WAYLAND_CLIENT_H

#include <wayland-server.h>

namespace phasor {
namespace wayland {

class Client
{
public:
	Client(wl_client *client);

	wl_client *_wl_client() const { return m_wl_client; }
	void set_wl_client(wl_client *r) {
		m_wl_client = r;
	}

	wl_listener *wl_destroy_listener() {
		return &m_destroy_listener;
	}

protected:
	wl_client *m_wl_client;

	// This needs to be public so that we can use Wayland's wl_container_of()
public:
	struct wl_listener m_destroy_listener;
	/// @todo add wl_keyboard and wl_pointer
};

}
}

#endif // CLIENT_H
