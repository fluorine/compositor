#include "surface.h"
using namespace phasor::wayland;

Surface::Surface(){

}


void Surface::frame_done()
{
	if (m_wl_frame_callback != nullptr) {
		wl_callback_send_done(m_wl_frame_callback, 0);
		// The Wayland protocol states that the notification shall be posted
		// once and needs to be requested again for future notifications.
		m_wl_frame_callback = nullptr;
	}
}

void Surface::send_enter()
{
	///@todo Haven't implemented outputs yet.
}

void Surface::send_leave()
{
	///@todo Haven't implemented outputs yet.
}
