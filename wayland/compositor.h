#ifndef WLCOMPOSITOR_H
#define WLCOMPOSITOR_H

#include <wayland-server.h>
#include <vector>

#include "wayland/client.h"

namespace phasor {
namespace wayland {

class Compositor
{
public:
	Compositor();
	~Compositor();
	void create_display();

	void dispatch_events();

protected:
	wl_display *m_display;
	wl_event_loop *m_event_loop;

	std::vector<Client> clients;
};

}
}

#endif // WLCOMPOSITOR_H
