#include "wayland/compositor.h"
#include "protocols/xdg_shell_v6.h"
#include "geometry/region.h"
#include <stdio.h>
#include <stdexcept>
#include <assert.h>

namespace pg = phasor::geometry;
using namespace phasor::wayland;

static void surface_destroy(wl_client *client, wl_resource *resource)
{
	/// @todo
}

static void surface_attach(wl_client *client, wl_resource *resource,
                           wl_resource *buffer, int32_t x, int32_t y)
{
	/// @todo
}

static void surface_damage(wl_client *client, wl_resource *resource,
                           int32_t x, int32_t y, int32_t width, int32_t height)
{
	/// @todo
}

static void surface_frame(wl_client *client, wl_resource *resource, uint32_t callback)
{
	/// @todo
}

static void surface_set_opaque_region(wl_client *client, wl_resource *resource, wl_resource *region)
{
	/// @todo
}

static void surface_set_input_region(wl_client *client, wl_resource *resource, wl_resource *region)
{
	/// @todo
}

static void surface_commit(wl_client *client, wl_resource *resource)
{
	/// @todo
}

static void surface_set_buffer_transform(wl_client *client, wl_resource *resource,
                                         int32_t transform)
{
	/// @todo
}

static void surface_set_buffer_scale(wl_client *client, wl_resource *resource, int32_t scale)
{
	/// @todo
}

static void surface_damage_buffer(wl_client *client, wl_resource *resource,
                                  int32_t x, int32_t y, int32_t width, int32_t height)
{
	/// @todo
}

#define WL_SURFACE_IFACE_VERSION 4
struct wl_surface_interface this_surface_interface = {
	surface_destroy,
	surface_attach,
	surface_damage,
	surface_frame,
	surface_set_opaque_region,
	surface_set_input_region,
	surface_commit,
	surface_set_buffer_transform,
	surface_set_buffer_scale,
	surface_damage_buffer
};

static void compositor_create_surface(wl_client *client, wl_resource *resource, uint32_t id)
{
	wl_resource *surface = wl_resource_create(client, &wl_surface_interface,
	                                          WL_SURFACE_IFACE_VERSION, id);
	wl_resource_set_implementation(surface, &this_surface_interface, nullptr, nullptr);
	/// @todo Attach the surface class returned by the compositor.
}

auto get_region(struct wl_resource *resource) -> pg::Region*
{
	pg::Region *region = static_cast<pg::Region*>(
	            wl_resource_get_user_data(resource));
	assert(region != nullptr);
	return region;
}

static void region_destroy(struct wl_client *, struct wl_resource *resource)
{
	pg::Region *region = get_region(resource);
	delete region;
}

static void region_add(struct wl_client *,
                       struct wl_resource *resource,
                       int32_t x, int32_t y, int32_t width, int32_t height)
{
	pg::Rectangle rect(pg::Point {x, y}, pg::Size {width, height});
	pg::Region *region = get_region(resource);
	(*region) += rect;
}

static void region_subtract(struct wl_client *,
                            struct wl_resource *resource,
                            int32_t x, int32_t y, int32_t width, int32_t height)
{
	pg::Rectangle rect(pg::Point {x, y}, pg::Size {width, height});
	pg::Region *region = get_region(resource);
	(*region) -= rect;
}

static struct wl_region_interface region_interface = {
	&region_destroy,
	&region_add,
	&region_subtract
};

static void compositor_create_region(wl_client *c,
                                     wl_resource *,
                                     uint32_t id)
{
	wl_resource *region = wl_resource_create(c, &wl_region_interface, 1, id);
	wl_resource_set_implementation(region, &region_interface, NULL, NULL);
	wl_resource_set_user_data(region, static_cast<void*>(new pg::Region()));
}

#define WL_COMPOSITOR_IFACE_VERSION 4
struct wl_compositor_interface this_compositor_interface = {
	compositor_create_surface,
	compositor_create_region
};

static void compositor_bind(wl_client *client, void *data,
                            uint32_t version, uint32_t id)
{
	wl_resource *resource = wl_resource_create(client,
	                                           &wl_compositor_interface,
	                                           WL_COMPOSITOR_IFACE_VERSION, id);
	wl_resource_set_implementation(resource, &this_compositor_interface, nullptr, nullptr);
	wl_resource_set_user_data(resource, data);

}

static void zxdg_shell_destroy(wl_client *client, wl_resource *resource)
{
	/// @todo
}

static void zxdg_shell_create_positioner(wl_client *client, wl_resource *resource, uint32_t id)
{
	/// @todo
}

static void zxdg_shell_get_xdg_surface(wl_client *client, wl_resource *resource, uint32_t id,
                                       wl_resource *surface)
{
	/// @todo
	printf("Create XDG surface called.\n");
}

static void zxdg_shell_pong(wl_client *client, wl_resource *resource, uint32_t serial)
{
	/// @todo
}

struct zxdg_shell_v6_interface this_xdg_shell_v6_interface = {
	zxdg_shell_destroy,
	zxdg_shell_create_positioner,
	zxdg_shell_get_xdg_surface,
	zxdg_shell_pong,
};

static void zxdg_shell_bind(wl_client *client, void *data, uint32_t version, uint32_t id)
{
	wl_resource *resource = wl_resource_create(client,
	                                           &zxdg_shell_v6_interface,
	                                           1, id);
	wl_resource_set_implementation(resource, &this_xdg_shell_v6_interface, nullptr, nullptr);
	wl_resource_set_user_data(resource, data);
}

Compositor::Compositor()
{

}

Compositor::~Compositor()
{
	wl_display_destroy(m_display);
}

void Compositor::create_display()
{
	m_display = wl_display_create();
	wl_display_add_socket_auto(m_display);
	wl_global_create(m_display, &wl_compositor_interface, WL_COMPOSITOR_IFACE_VERSION, (void*)this,
	                 &compositor_bind);
	wl_global_create(m_display, &zxdg_shell_v6_interface, 1, (void*)this, &zxdg_shell_bind);

	wl_display_init_shm(m_display);
	m_event_loop = wl_display_get_event_loop(m_display);
}

void Compositor::dispatch_events()
{
	wl_event_loop_dispatch(m_event_loop, 0);
	wl_display_flush_clients(m_display);
}
