#ifndef PHASOR_WAYLAND_SURFACE_H
#define PHASOR_WAYLAND_SURFACE_H

#include <wayland-server.h>
#include <memory>

#include "geometry/size.h"
#include "geometry/region.h"
#include "wayland/client.h"

namespace phasor {
namespace wayland {

class Surface
{
public:
	Surface();

	wl_resource *wl_surface() const { return m_wl_surface; }
	wl_resource *xdg_surface() const { return m_xdg_surface; }
	wl_resource *wl_buffer() const { return m_wl_buffer; }
	wl_resource *wl_frame_callback() const {
		return m_wl_frame_callback;
	}

	auto client() -> std::weak_ptr<Client> { return m_client; }

	bool is_damaged() const { return m_is_damaged; }
	geometry::Region input_region() const {
		return m_input_region;
	}
	phasor::geometry::Region opaque_region() const {
		return m_opaque_region;
	}
	int buffer_scale() const { return m_buffer_scale; }

	void set_wl_surface(wl_resource *r) {
		m_wl_surface = r;
	}

	void set_xdg_surface(wl_resource *r) {
		m_xdg_surface = r;
	}

	void set_wl_buffer(wl_resource *r) {
		m_wl_buffer = r;
	}

	void set_wl_frame_callback(wl_resource *r) {
		m_wl_frame_callback = r;
	}

	void set_client(std::weak_ptr<Client> c) {
		m_client = c;
	}

	void set_damaged(bool damaged = true) {
		m_is_damaged = damaged;
	}

	void set_input_region(phasor::geometry::Region r) {
		m_input_region = r;
	}

	void set_opaque_region(phasor::geometry::Region r) {
		m_opaque_region = r;
	}

	void set_buffer_scale(int scale) {
		m_buffer_scale = scale;
	}

	virtual void commit() = 0;
	virtual void frame_done();

	void send_enter();
	void send_leave();

protected:
	wl_resource *m_wl_surface = nullptr;
	wl_resource *m_xdg_surface = nullptr;
	wl_resource *m_wl_buffer = nullptr;
	wl_resource *m_wl_frame_callback = nullptr;

	std::weak_ptr<Client> m_client;

	bool m_is_damaged = false;
	phasor::geometry::Region m_input_region;
	phasor::geometry::Region m_opaque_region;
	int m_buffer_scale;

};

}
}

#endif // PHASOR_WAYLAND_SURFACE_H
